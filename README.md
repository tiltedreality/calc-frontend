# LoanPro Calculator Frontend

This template should help get you started developing with Vue 3 in Vite.

## Running

To install dependencies and get a server run:

```sh
npm install
npm run dev
```

The output will contain a url you can use to access the running server.

## Discussion

### Using the calculator

In order to use the calculator you can either type in the text box or use the buttons. Pressing Enter or = will cause the operation to be processed (if all the required information has been filled out). When a result is being displayed starting to type or pressing a button will clear the result and start setting up the next operation. Note that using to use the square root you _first_ type the number, then press the √ button or r key. Getting a random string can only be accessed via the button.

All operations use floating point on the backend, and comes with all the expected limitations of floating point.

### Deployment

For pragmatism's sake I deployed to infrastructure I already own. The full Kubernetes manifest can be found in `kube/deploy/calc-frontend.yml`. It is automatically built and deployed by the CI pipeline defined in `.gitlab-ci.yml`.

### User management

For convience is evaluation I have included the users current balance, as well as the ability to set the balance on the records page. Setting the balance does not add a record, it exists only to aid in evaluation.
