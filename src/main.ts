import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router'
import { handleError } from '@/errors'

import "bootstrap/dist/css/bootstrap.css"
import "bootstrap"

const app = createApp(App)

app.use(router)

app.mount('#app')

app.config.errorHandler = (err, vm, info) => {
    console.log("Vue Error handler")
    handleError(err)
}
window.onerror = handleError;
window.onunhandledrejection = handleError