import router from '@/router';
import axios, { type AxiosResponse } from 'axios';
import { reactive } from 'vue';


export let auth: { token: string | null } = reactive({ token: null });

if (import.meta.hot) {
    if (import.meta.hot.data.auth) {
        auth = import.meta.hot.data.auth;
    }

    import.meta.hot.on('vite:beforeUpdate', () => {
        if (import.meta.hot) {
            import.meta.hot.data.auth = auth
        }
    });
}

const baseURL = import.meta.env.PROD ? 'https://calc-api.tiltedreality.com/api/v1' : 'http://localhost:3000/api/v1';

export interface TokenResponse {
    token: string;
}

export interface UserBalance {
    balance: number;
}

export interface RandomString {
    result: string;
}

export interface MathOperation {
    total: number;
}

export interface LoginCredentials {
    username: string;
    password: string;
}

interface APIRecord {
    id: number;
    operation: string;
    amount: number;
    user_balance: number;
    response: string;
    created_at: string;
}

interface APIRecordResponse {
    pages: number;
    records: APIRecord[];
}


export interface Record {
    id: number;
    operation: string;
    amount: number;
    user_balance: number;
    response: string;
    created_at: Date;
}

export interface PagedRecords {
    pages: number;
    records: Record[];
}

export interface XY {
    x: number;
    y: number;
}

export class UnauthenticatedError extends Error {
    constructor(message: string) {
        super(message);
        this.name = "UnauthenticatedError";
    }
}

export class InsufficientTokensError extends Error {
    constructor(message: string) {
        super(message);
        this.name = "InsufficientTokensError";
    }
}

function handleError<T>(error: any): Promise<AxiosResponse<T>> {
    if (axios.isAxiosError(error)) {
        switch (error.response?.status) {
            case 401:
                auth.token = null;
                router.push({ name: 'login' });
                throw new UnauthenticatedError("Unauthenticated, Please login");
            case 402:
                throw new InsufficientTokensError("Insufficient tokens");
            default:
                throw error;
        }
    } else {
        throw error;
    }
}

async function post<T>(url: string, data: any): Promise<AxiosResponse<T>> {
    try {
        return await axios.post(`${baseURL}${url}`, data, { headers: { Authorization: `Bearer ${auth.token}` } });
    } catch (error) {
        return handleError(error);
    }
}

async function get<T>(url: string, queryParams: any = null): Promise<AxiosResponse<T>> {
    const config = {
        headers: { Authorization: `Bearer ${auth.token}` },
        params: queryParams
    }
    try {
        return await axios.get(`${baseURL}${url}`, config);
    } catch (error) {
        return handleError(error);
    }
}


export async function login(credentials: LoginCredentials): Promise<boolean> {
    try {
        const response: AxiosResponse<TokenResponse> = await axios.post(`${baseURL}/token`, credentials);
        auth.token = response.data.token;
        return true;
    } catch (error) {
        if (axios.isAxiosError(error) && error.response?.status === 401) {
            return false;
        } else {
            throw error;
        }
    }
}

export async function add(x: number, y: number): Promise<number> {
    const response: AxiosResponse<MathOperation> = await post('/math/plus', { x, y });
    return response.data.total;
}

export async function subtract(x: number, y: number): Promise<number> {
    const response: AxiosResponse<MathOperation> = await post('/math/minus', { x, y });
    return response.data.total;
}

export async function multiply(x: number, y: number): Promise<number> {
    const response: AxiosResponse<MathOperation> = await post('/math/multiply', { x, y });
    return response.data.total;
}

export async function divide(x: number, y: number): Promise<number> {
    const response: AxiosResponse<MathOperation> = await post('/math/divide', { x, y });
    return response.data.total;
}

export async function root(x: number): Promise<number> {
    const response: AxiosResponse<MathOperation> = await post('/math/root', { x });
    return response.data.total;
}

export async function randomString(): Promise<string> {
    const response: AxiosResponse<RandomString> = await post('/string/random', {});
    return response.data.result;
}

export async function userBalance(): Promise<number> {
    const response: AxiosResponse<UserBalance> = await get('/user/balance');
    return response.data.balance;
}

export async function setUserBalance(balance: number): Promise<void> {
    await post('/user/balance', { balance });
}

export async function getRecords(search: string | null, sort: string, desc: boolean, page: number, pageSize: number): Promise<PagedRecords> {
    const queryParams = { search, sort, desc, page, "page-size": pageSize }
    const response: AxiosResponse<APIRecordResponse> = await get('/user/records', queryParams);
    const records = response.data.records.map((record: APIRecord) => {
        return {
            id: record.id,
            operation: record.operation,
            amount: record.amount,
            user_balance: record.user_balance,
            response: record.response,
            created_at: new Date(record.created_at),
        }
    });
    return { pages: response.data.pages, records };
}

export async function deleteRecord(id: number): Promise<boolean> {
    await axios.delete(`${baseURL}/user/record/${id}`, { headers: { Authorization: `Bearer ${auth.token}` } });
    return true;
}