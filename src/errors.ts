import { reactive } from "vue";
import { UnauthenticatedError } from "@/api";

export const error: { message: string | null; } = reactive({ message: null });

export function handleError(e: any) {
    console.error("Handling uncaught error")
    console.error(e);
    if (e instanceof UnauthenticatedError || (e.reason && e.reason instanceof UnauthenticatedError)) {
        error.message = "You are not logged in. Please log in and try again."
    } else {
        error.message = "An uknown error has occured. Please contact me@waltonhoops.com."
    }
}